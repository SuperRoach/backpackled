# This plays the newest Video on the screen
#!/bin/bash
latestfile=$(ls -t *.jpg | head -1)
#echo $latestfile

sudo /home/pi/rpi-rgb-led-matrix/utils/led-image-viewer --led-cols=64 --led-pixel-mapper="U-mapper;Rotate:-180"  -C ~/$latestfile

