# This plays the newest gif on the screen
#!/bin/bash
latestfile=$(ls -t *.gif | head -1)
#echo $latestfile

sudo /home/pi/rpi-rgb-led-matrix/utils/led-image-viewer --led-pixel-mapper="U-mapper;Rotate:-180" --led-cols=64 -C ~/$latestfile

#unset -v latest
#for file in "~"/*; do
#  [[ $file -nt $latest ]] && latest=$file
#done
#echo $latest
#sudo /home/pi/rpi-rgb-led-matrix/utils/led-image-viewer --led-cols=64 -C $file
