# This plays the newest Video on the screen, but it's using -f to also be able to loop it (its intensive, check https://github.com/hzeller/rpi-rgb-led-matrix/issues/620 for a better option?
#!/bin/bash
latestfile=$(ls -t *.mp4 | head -1)
#echo $latestfile

#sudo /home/pi/rpi-rgb-led-matrix/utils/led-image-viewer --led-cols=64 -C ~/$latestfile
sudo /home/pi/rpi-rgb-led-matrix/utils/video-viewer --led-pixel-mapper="U-mapper;Rotate:-180" -f --led-cols=64 ~/$latestfile
