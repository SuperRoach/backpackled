# This plays the newest Video on the screen
#!/bin/bash
latestfile=$(ls -t *.mp4 | head -1)
#echo $latestfile

#sudo /home/pi/rpi-rgb-led-matrix/utils/led-image-viewer --led-cols=64 -C ~/$latestfile
sudo /home/pi/rpi-rgb-led-matrix/utils/video-viewer --led-pixel-mapper="U-mapper;Rotate:-180" --led-cols=64 ~/$latestfile
